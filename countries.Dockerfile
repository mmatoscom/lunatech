FROM openjdk:8-jre-alpine
LABEL maintainer Marco Matos marco@mmatos.com

    ENV APP countries
    ENV VERSION 1.0.1

    WORKDIR /
    RUN wget https://s3-eu-west-1.amazonaws.com/devops-assesment/${APP}-assembly-${VERSION}.jar
    
    #COPY ${APP}-assembly-${VERSION}.jar /
    CMD ["/usr/bin/java", "-jar", "/countries-assembly-1.0.1.jar"]


