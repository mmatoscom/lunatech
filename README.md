** LUNATECH challenge

<br>

I assume you have docker, docker-compose and make installed on your linux system

** TODO list
<br>
- create a separate dockerfile for each app - ok
- pay attention on how CI/CD will deal with APP versions
- create a docker-compose file in order to bring stack services up all together - ok
- create Makefile to make it easier to deal with the docker commands - ok
- create gitlab-ci.yml file to setup gitlab autodevops CI/CD

<br>
* how does it work: 
- by clonning this repo, you can build all images and publish stack with ```make``` command, <br>
- there are few commands worth to mention, as: <br>
```make build``` builds all images, pulling latest base image and builds with no cache <br>
```make stack``` runs the docker-compose up command, bringing up all stack together <br>
```make deploy``` builds and bring stack up as well, same as ```make``` without arguments <br>
```make runner``` creates the volume for gitlab-runners settings, creates runner container and registers it to this repo. 


