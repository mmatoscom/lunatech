# Marco Matos  <marco@mmatos.com>

NAME = m4rc0/lunatech
TIMESTAMP:= `date +%Y`-`date +%m`-`date +%d`-`date +%s`
BRANCH:= $$(git branch)
REVISION:= $$(git rev-parse HEAD)
USER:= $$(git config user.name)
VERSION:= 1.0.1
CONTAINER_NAME:= lunatech
URL:= http://localhost:8000

.PHONY: all clean backup rm airport countries stack deploy runner

all: deploy 

clean: 
	@echo 'Removing old releases...'
	rm -rf release-*

backup: clean
	@echo 'Performing Backup...'
	@echo 'Creating new release...'
	@printf "\nDate: $(TIMESTAMP)\nBranch: $(BRANCH)\nRevision: $(REVISION)\nDeployed by: $(USER)\n\n" > rev.txt
	zip -r release-$(TIMESTAMP) ./*
	@echo Release release-$(TIMESTAMP).zip created.

rm:
	docker-compose stop 
	docker-compose rm -f

airport:
	cat airport.Dockerfile > Dockerfile
	docker build --no-cache --pull -t m4rc0/lunatech:airport-service .

countries:
	cat countries.Dockerfile > Dockerfile
	docker build --no-cache --pull -t m4rc0/lunatech:country-service .

proxy:
	cat reverse-proxy.Dockerfile > Dockerfile
	docker build --no-cache --pull -t m4rc0/lunatech:reverse-proxy .

build: airport countries proxy 

push:
	docker push m4rc0/lunatech:airport-service
	docker push m4rc0/lunatech:country-service
	docker push m4rc0/lunatech:reverse-proxy

release: build push

stack:
	docker-compose up -d 

deploy: build stack 

runner:
	docker run -d --name gitlab-runner-config -v /etc/gitlab-runner busybox:latest /bin/true
	docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock --volumes-from gitlab-runner-config gitlab/gitlab-runner:alpine
	docker exec gitlab-runner gitlab-runner register --non-interactive --url "https://gitlab.com/" --registration-token ${PROJECT_REGISTRATION_TOKEN} --executor "docker" --docker-image alpine:3 --description "lunatech-runner" --tag-list "docker,aws,lunatech"  --run-untagged --locked="false"